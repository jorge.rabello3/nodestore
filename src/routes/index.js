"use strict";

const express = require("express");
const router = express.Router();

router.get("/health", (_req, res, _next) => {
  res.status(200).send({
    title: "Node Store API",
    version: "0.1.3",
    health: "OK",
    status: "up",
  });
});

module.exports = router;

"use strict";

const express = require("express");

const app = express();

const bodyParser = require("body-parser");

const router = express.Router();

// carrega as rotas
const index = require("./routes/index");
const product = require("./routes/product");

const swaggerUi = require("swagger-ui-express");
const swaggerDocument = require("./swagger/swagger.json");

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use("/", index);
app.use("/products", product);
app.use("/", swaggerUi.serve, swaggerUi.setup(swaggerDocument));

module.exports = app;
